﻿namespace YogoPop.Demo.Infrastructure;

public abstract class YogoPopDemoSettings : StartupSettings
{
    public SwaggerSettings SwaggerSettings { get; set; }
}