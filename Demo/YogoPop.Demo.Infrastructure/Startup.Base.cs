﻿namespace YogoPop.Demo.Infrastructure;

public abstract class YogoPopDemoStartupBase<TStartupSettings> : StartupBase<TStartupSettings, AppConfigure>
    where TStartupSettings : YogoPopDemoSettings
{
    public YogoPopDemoStartupBase(IConfiguration configuration) : base(configuration) { }

    protected override JsonSerializerSettings SetJsonSerializerSettings()
    {
        return new JsonSerializerSettings()
        {
            Formatting = Formatting.None,
            //DateFormatString = "yyyy-MM-dd HH:mm:ss.fff",
            DateFormatString = "yyyy-MM-dd HH:mm:ss",
            //ContractResolver = new DefaultContractResolver(),
            ContractResolver = new CamelCasePropertyNamesContractResolver(),
            Converters = new List<JsonConverter> {
                    new StringEnumConverter(),
            },
        };
    }

    protected void RegisEF(ContainerBuilder containerBuilder)
    {
        //var efOptionsBuilder = new EFDBContextOptionsBuilder<EFDBContext_HousingSteward>()
        //{
        //    BulidAction = (optionsBuilder) =>
        //    {
        //        optionsBuilder.UseSqlServer(InjectionContext.Resolve<DBConnectionSettings>().HousingSteward);

        //        return optionsBuilder.Options;
        //    }
        //};
        //containerBuilder.Register(o => efOptionsBuilder).AsSelf().InstancePerDependency();

        //containerBuilder.RegisterType<EFDBContext_HousingSteward>().As<IDBContext>().InstancePerDependency();
        //containerBuilder.RegisterType<EFDBContext_HousingSteward>().As<EFDBContext_HousingSteward>().InstancePerDependency();
        //containerBuilder.RegisterType<EFDBContext_HousingSteward>().Keyed<IDBContext>(ORMTypEnum.EF).InstancePerDependency();
        //containerBuilder.RegisterType<EFDBContext_HousingSteward>().Keyed<EFDBContext_HousingSteward>(ORMTypEnum.EF).InstancePerDependency();
    }

    protected void RegisSqlSugar(ContainerBuilder containerBuilder)
    {
        //var ssOptionBuilder = new SSDBContextOptionBuilder()
        //{
        //    BulidAction = (connectionConfig) =>
        //    {
        //        connectionConfig.DbType = SqlSugar.DbType.SqlServer;
        //        connectionConfig.ConnectionString = InjectionContext.Resolve<DBConnectionSettings>().HousingSteward;

        //        return connectionConfig;
        //    }
        //};
        //containerBuilder.Register(o => ssOptionBuilder).AsSelf().InstancePerDependency();

        //var ssOptionsBuilder = new SSDBContextOptionsBuilder()
        //{
        //    BulidAction = (connectionConfigs) =>
        //    {
        //        foreach (var connectionConfig in connectionConfigs)
        //        {
        //            connectionConfig.DbType = SqlSugar.DbType.SqlServer;
        //            connectionConfig.ConnectionString = InjectionContext.Resolve<DBConnectionSettings>().HousingSteward;
        //        }

        //        return connectionConfigs;
        //    }
        //};
        //containerBuilder.Register(o => ssOptionsBuilder).AsSelf().InstancePerDependency();
    }
}