namespace YogoPop.Demo.Infrastructure;

public abstract class YogoPopDemoStartupOnWeb<TStartupSettings> : YogoPopDemoStartupBase<TStartupSettings>
    where TStartupSettings : YogoPopDemoWebApiSettings
{
    public YogoPopDemoStartupOnWeb(IConfiguration configuration) : base(configuration) { }

    public void Configure(IApplicationBuilder app, IHostEnvironment env, IHostApplicationLifetime lifetime, ILoggerFactory loggerFactory, IApiVersionDescriptionProvider apiVerDescProvider)
    {
        var configures = new AppConfigure()
        {
            App = app,
            Env = env,
            Lifetime = lifetime,
            LoggerFactory = loggerFactory,
            ApiVerDescProvider = apiVerDescProvider,
        };

        Configure(configures);
    }

    protected override void Extend_ConfigureServices(IServiceCollection services)
    {
        services.AddTransient<IHttpContextAccessor, HttpContextAccessor>();
        services
            .Configure<ApiBehaviorOptions>(options =>
            {
                //options.SuppressModelStateInvalidFilter = true;
            })
            .AddCors()
            .AddRouting(options =>
            {
                options.LowercaseUrls = true;
            })
            .AddControllers(options =>
            {
                options.Filters.Add<CtrlerFilterAttribute>();
            })
            .AddControllersAsServices()
            .AddJsonOptions(options =>
            {
                if (!(JsonSerializerSettings.ContractResolver is CamelCasePropertyNamesContractResolver))
                    options.JsonSerializerOptions.PropertyNamingPolicy = null;
            })
            .AddNewtonsoftJson(options =>
            {
                options.SerializerSettings.Formatting = JsonSerializerSettings.Formatting;
                options.SerializerSettings.DateFormatString = JsonSerializerSettings.DateFormatString;
                options.SerializerSettings.ContractResolver = JsonSerializerSettings.ContractResolver;
                options.SerializerSettings.Converters = JsonSerializerSettings.Converters;
            });

        services.AddSwagger(CurConfig.SwaggerSettings);
    }

    protected override void Extend_ConfigureContainer(ContainerBuilder containerBuilder)
    {
        containerBuilder.RegisSeriLogger(Configuration);
        containerBuilder.RegisRedis<SERedis>();
        containerBuilder.RegisStateCodeValueConverter<StateCodeEnum>();

        RegisEF(containerBuilder);
    }

    protected override void Extend_Configure(AppConfigure configures)
    {
        if (configures.Env.IsDevelopment())
        {
            configures.App.UseDeveloperExceptionPage();
        }

        configures.App.UseCors(options =>
        {
            options.AllowAnyHeader();
            options.AllowAnyMethod();
            options.SetIsOriginAllowed(o => true);
            options.AllowCredentials();
        });

        configures.UseSwagger(CurConfig.SwaggerSettings);

        configures.App.UseRouting();
        configures.App.UseAuthorization();
        configures.App.UseEndpoints(endpoints =>
        {
            endpoints.MapControllers();
        });
    }
}