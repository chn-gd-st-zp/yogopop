namespace YogoPop.Demo.Infrastructure;

public class YogoPopDemoWebApiSettings : YogoPopDemoSettings
{
    public RedisSettings RedisSettings { get; set; }
}