namespace YogoPop.Demo.Host.WebApi;

public class Startup : YogoPopDemoStartupOnWeb<YogoPopDemoWebApiSettings>
{
    public Startup(IConfiguration configuration) : base(configuration) { }

    protected override void Extend_ConfigureServices(IServiceCollection services)
    {
        base.Extend_ConfigureServices(services);
    }

    protected override void Extend_ConfigureContainer(ContainerBuilder containerBuilder)
    {
        base.Extend_ConfigureContainer(containerBuilder);

        containerBuilder.RegisRedis<FRedis, MultilangSettings>();
        containerBuilder.MultilangFromFile();
    }

    protected override void Extend_Configure(AppConfigure configures)
    {
        base.Extend_Configure(configures);
    }
}