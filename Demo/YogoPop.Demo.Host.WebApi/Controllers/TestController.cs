namespace YogoPop.Demo.Host.WebApi.Controllers;

/// <summary>
/// 多语言测试
/// </summary>
[ApiVersion("1.0", Deprecated = false)]
[ApiController, Route("api/[controller]"), Route("api/v{version:apiVersion}/[controller]")]
[LogIgnore, AllowAnonymous]
[CustomizeHeader(AppInitHelper.LanguageKeyInHeader)]
public class MLTestController : ControllerBase
{
    /// <summary>
    /// 映射
    /// </summary>
    /// <param name="input"></param>
    /// <returns></returns>
    [HttpPost, Route("Mapping")]
    public async Task<IApiResult<string>> Mapping(DToMultilangMappingInput input)
    {
        var result = "";

        var contentData = InjectionContext.Resolve<IMultilangExchanger>().GetAsync(input.Language.ToString(), input.GroupKey, input.ItemKey).Result;
        result = contentData != null ? contentData.DestContent : "";

        return result.Success().ToApiResult();
    }

    /// <summary>
    /// 模拟登录
    /// </summary>
    /// <param name="input"></param>
    /// <returns></returns>
    [HttpPost, Route("LoginSimulate")]
    public async Task<IApiResult<bool>> LoginSimulate(DToMultilangLoginSimulateInput input) => false.Fail<bool, PasswordError>().ToMLApiResult();

    /// <summary>
    /// 语言
    /// </summary>
    [Description("语言")]
    [PublicEnum]
    public enum LanguageEnum
    {
        /// <summary>
        /// 默认、无
        /// </summary>
        [Description("默认、无")]
        None = 0,

        /// <summary>
        /// 未知，未定义
        /// </summary>
        [Description("未知，未定义")]
        Unknown = 1,

        /// <summary>
        /// 中文
        /// </summary>
        [Description("中文")]
        CN,

        /// <summary>
        /// 英文
        /// </summary>
        [Description("英文")]
        EN,

        /// <summary>
        /// 泰文
        /// </summary>
        [Description("泰文")]
        TH,
    }

    public class DToMultilangMappingInput : DTOInput
    {
        [Required]
        public string GroupKey { get; set; }

        [Required]
        public string ItemKey { get; set; }

        [Required]
        public LanguageEnum Language { get; set; }
    }

    public class DToMultilangLoginSimulateInput : DTOInput
    {
        public string UserName { get; set; }

        public string Password { get; set; }

        public override bool Validation(out string errorMsg)
        {
            errorMsg = "";

            //if (UserName.IsEmptyString())
            //{
            //    errorMsg = "cn".GetDestContent<UserNameEmptyError>();
            //    return false;
            //}

            //if (Password.IsEmptyString())
            //{
            //    errorMsg = "cn".GetDestContent<PasswordEmptyError>();
            //    return false;
            //}

            return true;
        }
    }

    [Description("提示信息-用户名为空时")]
    public class UserNameEmptyError : IMultiLanguageObject
    {
        public string GroupKey => "SysAlert";

        public string ItemKey => "000001";
    }

    [Description("提示信息-密码为空时")]
    public class PasswordEmptyError : IMultiLanguageObject
    {
        public string GroupKey => "SysAlert";

        public string ItemKey => "000002";
    }

    [Description("提示信息-当输入的密码错误时")]
    public class PasswordError : IMultiLanguageObject
    {
        public string GroupKey => "SysAlert";

        public string ItemKey => "000003";
    }
}