﻿namespace YogoPop.Component.Dispatcher;

public class QuartzTimingSettings : TimingSettings<TimingParams> { }

public interface IQuartzTimingRegister : ITimingRegister<TimingParams> { }

//[DisallowConcurrentExecution]
public interface IQuartzTimingDispatcher : ITimingDispatcher, IJob { }

[DIModeForService(DIModeEnum.ExclusiveByKeyed, typeof(QuartzTimingDispatcher), DispatcherTypeEnum.QuartzTiming)]
public class QuartzTimingDispatcher : Dispatcher
{
    protected override void DoWork()
    {
        var settings = InjectionContext.Resolve<DispatcherSettings>();

        if (settings.QuartzTiming.IsNotEmpty())
        {
            foreach (var timer in settings.QuartzTiming)
            {
                foreach (var item in timer.Items)
                {
                    var register = InjectionContext.Resolve<IQuartzTimingRegister>();
                    var runner = InjectionContext.ResolveByNamed<IQuartzTimingDispatcher>(timer.Type);
                    register.Regis(runner, timer.Cron, item);
                }
            }
        }
    }
}

public class QuartzTimingRegister : IQuartzTimingRegister
{
    private IScheduler _scheduler { get; set; }

    public QuartzTimingRegister()
    {
        _scheduler = InjectionContext.Resolve<ISchedulerFactory>().GetScheduler().Result;
        _scheduler.JobFactory = InjectionContext.Resolve<IJobFactory>();

        _scheduler.Start().Wait();
    }

    public async Task Regis(ITimingDispatcher runner, string cron, TimingParams param)
    {
        var type = runner.GetType();

        var job = JobBuilder
            .Create(type)
            .WithIdentity(type.FullName)
            .WithDescription(param.Name)
            .Build();

        var trigger = TriggerBuilder
            .Create()
            .WithIdentity($"{type.FullName}.trigger")
            .WithCronSchedule(cron)
            .WithDescription(cron)
            .Build();

        await _scheduler.ScheduleJob(job, trigger);
    }
}

[AttributeUsage(AttributeTargets.Class, AllowMultiple = false)]
public class JobTypeAttribute : Attribute
{
    public Type Type { get; private set; }

    public JobTypeAttribute(Type type) { Type = type; }
}

public class JobFactory : IJobFactory
{
    public void ReturnJob(IJob job) { }

    //public IJob NewJob(TriggerFiredBundle bundle, IScheduler scheduler) => InjectionContext.ResolveByKeyed<IJob>(bundle.JobDetail.JobType);
    //public IJob NewJob(TriggerFiredBundle bundle, IScheduler scheduler) => InjectionContext.Resolve<IJob>();
    public IJob NewJob(TriggerFiredBundle bundle, IScheduler scheduler)
    {
        var attr = bundle.JobDetail.JobType.GetCustomAttribute<JobTypeAttribute>();
        if (attr == null || !attr.Type.IsImplementedOf<IJob>() || !attr.Type.IsImplementedOf<IQuartzTimingDispatcher>()) return null;

        return InjectionContext.Resolve(attr.Type) as IJob;
    }
}