﻿namespace YogoPop.Component.Dispatcher;

public class HangFireTimingSettings : TimingSettings<TimingParams> { }

public interface IHangFireTimingRegister : ITimingRegister<TimingParams> { }

//[DisableConcurrentExecution(30)]
public interface IHangFireTimingDispatcher : ITimingDispatcher { }

[DIModeForService(DIModeEnum.ExclusiveByKeyed, typeof(HangFireTimingDispatcher), DispatcherTypeEnum.HangFireTiming)]
public class HangFireTimingDispatcher : Dispatcher
{
    protected override void DoWork()
    {
        var settings = InjectionContext.Resolve<DispatcherSettings>();

        if (settings.HangFireTiming.IsNotEmpty())
        {
            foreach (var timer in settings.HangFireTiming)
            {
                foreach (var item in timer.Items)
                {
                    var register = InjectionContext.Resolve<IHangFireTimingRegister>();
                    var runner = InjectionContext.ResolveByNamed<IHangFireTimingDispatcher>(item.Type.IsNotEmptyString() ? item.Type : timer.Type);
                    register.Regis(runner, timer.Cron, item);
                }
            }
        }
    }
}

public class HangFireTimingRegister : IHangFireTimingRegister
{
    public Task Regis(ITimingDispatcher runner, string cron, TimingParams param)
    {
        RecurringJob.AddOrUpdate(param.Name, () => runner.Run(param.Name, param.Args), cron, TimeZoneInfo.Local);
        return Task.CompletedTask;
    }
}