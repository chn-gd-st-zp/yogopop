namespace YogoPop.Component.Dispatcher;

public class TimingParams
{
    public string Type { get; set; }

    public string Name { get; set; }

    public string[] Args { get; set; }
}

public class TimingSettings<TTimingParams> : DispatcherSettingsItem where TTimingParams : TimingParams
{
    public string Cron { get; set; }

    public List<TTimingParams> Items { get; set; }
}

public interface ITimingRegister<TTimingParams> where TTimingParams : TimingParams
{
    Task Regis(ITimingDispatcher runner, string cron, TTimingParams param);
}

public interface ITimingDispatcher : IDispatcher { }