﻿namespace YogoPop.Component.Swagger;

[AttributeUsage(AttributeTargets.Method | AttributeTargets.Class)]
public partial class ApiHiddenAttribute : Attribute { }