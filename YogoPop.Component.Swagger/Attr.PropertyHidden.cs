﻿namespace YogoPop.Component.Swagger;

[AttributeUsage(AttributeTargets.Property | AttributeTargets.Field)]
public partial class PropertyHiddenAttribute : Attribute { }