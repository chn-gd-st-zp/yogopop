﻿namespace YogoPop.Component.Swagger;

public static class SwaggerExtension
{
    internal static string GetCustomSchemaId(this string key)
    {
        var fullName = key.SplitRemoveEmptyEntries('|');
        return fullName[1] + "," + fullName[0];
        //return key;
    }

    internal static string CustomSchemaIdSelector(this Type type)
    {
        return type.Assembly.GetName() + "|" + type.FullName;
        //return type.FullName;
    }

    internal static Tuple<string, OpenApiSchema> GetSwaggerProperty(this OpenApiSchema schema, string propertyName)
    {
        var inputPropertyKey = string.Empty;
        var inputPropertySchema = default(OpenApiSchema);

        //遍历所有字段名标识
        foreach (var key in schema.Properties.Keys)
        {
            //找出与字段匹配的字段名标识
            if (!propertyName.IsEquals(key))
                continue;

            inputPropertyKey = key;
            inputPropertySchema = schema.Properties[inputPropertyKey];

            //找到立刻跳出循环
            break;
        }

        //找不到字段信息
        if (inputPropertyKey.IsEmptyString() || inputPropertySchema == default)
            return null;

        return new Tuple<string, OpenApiSchema>(inputPropertyKey, inputPropertySchema);
    }
}