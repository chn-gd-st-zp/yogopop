﻿namespace YogoPop.Support;

public interface IRequestFilterHandle : ITransient
{
    IRequestFilterItems FilterItems { get; }
}