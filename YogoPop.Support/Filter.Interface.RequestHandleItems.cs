﻿namespace YogoPop.Support;

public interface IRequestFilterItems : IList<IRequestFilterItem>, ITransient
{
    //
}