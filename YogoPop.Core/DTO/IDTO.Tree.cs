﻿namespace YogoPop.Core.DTO;

public interface IDTOTreeItem : IDTO
{
    public string Text { get; }

    public string Value { get; }
}