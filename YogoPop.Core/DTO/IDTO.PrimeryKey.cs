﻿namespace YogoPop.Core.DTO;

public interface IDTOPrimaryKey : IDTO
{
    //
}

public interface IDTOPrimaryKey<T> : IDTOPrimaryKey
{
    public T PrimaryKey { get; set; }
}