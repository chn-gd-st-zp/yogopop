﻿namespace YogoPop.Core.DTO;

public interface IDTOListor<TDTOSort> : IDTOSearch<TDTOSort>
    where TDTOSort : IDTOSort
{
    //
}