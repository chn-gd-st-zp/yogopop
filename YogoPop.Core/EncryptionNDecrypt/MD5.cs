﻿namespace YogoPop.Core.EncryptionNDecrypt;

public class MD5
{
    public static string Encrypt(string text, int digit = 32)
    {
        string md5code = string.Empty;
        using (var md5 = System.Security.Cryptography.MD5.Create())
        {
            byte[] retVal = md5.ComputeHash(Encoding.UTF8.GetBytes(text));
            StringBuilder md5sb = new StringBuilder();
            for (int i = 0; i < retVal.Length; i++)
            {
                md5sb.Append(retVal[i].ToString("x2"));
            }
            md5code = md5sb.ToString();
        }

        if (digit == 16) //16位MD5加密（取32位加密的9~25字符）
        {
            return md5code.Substring(8, 16);
        }

        if (digit == 32) //32位加密
        {
            return md5code;
        }

        return md5code;
    }
}