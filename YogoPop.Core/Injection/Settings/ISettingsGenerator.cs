﻿namespace YogoPop.Core.Injection.Settings;

public interface ISettingsGenerator
{
    string GetSetting(IConfiguration config, string rootName);

    T GetSetting<T>(IConfiguration config, string rootName) where T : ISettings;

    T GetSetting<T>(IConfiguration config) where T : ISettings;

    object GetSetting(IConfiguration config, string rootName, Type type);
}