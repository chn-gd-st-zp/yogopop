﻿namespace YogoPop.Core.Interface;

public interface IYogoLogger
{
    void Info(string msg);

    void Info<T>(T obj);

    void Error(Exception exception);

    void Error(string msg, Exception exception = null);

    void Error<T>(T obj, Exception exception = null);
}

public interface IYogoLogger<TTrigger> : IYogoLogger
    where TTrigger : class
{
    //
}