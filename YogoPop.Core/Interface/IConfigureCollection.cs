﻿namespace YogoPop.Core.Interface;

public interface IConfigureCollection
{
    IApplicationBuilder App { get; set; }

    IHostEnvironment Env { get; set; }

    IHostApplicationLifetime Lifetime { get; set; }

    ILoggerFactory LoggerFactory { get; set; }

    IApiVersionDescriptionProvider ApiVerDescProvider { get; set; }
}