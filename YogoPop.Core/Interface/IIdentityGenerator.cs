﻿namespace YogoPop.Core.Interface;

public interface IIdentityGenerator
{
    public string Get();
}