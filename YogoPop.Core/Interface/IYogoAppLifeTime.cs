﻿namespace YogoPop.Core.Interface;

public interface IYogoAppLifeTime
{
    Task Started(params object[] args);

    Task Stopping(params object[] args);

    Task Stopped(params object[] args);
}