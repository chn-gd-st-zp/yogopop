﻿namespace YogoPop.Core.Result;

public interface IServiceResult<TData>
{
    bool IsSuccess { get; set; }

    Exception ExInfo { get; set; }

    IVEnumItem Code { get; set; }

    string Msg { get; set; }

    TData Data { get; set; }
}