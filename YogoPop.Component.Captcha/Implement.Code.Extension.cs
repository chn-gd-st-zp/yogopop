﻿namespace YogoPop.Component.Captcha;

public class CodeCaptchaGenerator : ICaptchaCodeGenerator
{
    public string Generate(int length) { return Unique.GetRandomCode5(length); }
}

public class CodeCaptchaGraphics : ICaptchaImageGenerator
{
    public byte[] Generate(int width, int height, string captchaCode)
    {
        var result = default(byte[]);

        var settings = InjectionContext.Resolve<CodeCaptchaSettings>();

        var codeW = settings.ImageWidth;
        var codeH = settings.ImageHeight;
        var fontSize = settings.FontSize;
        var fonts = settings.Fonts;
        var colors = settings.Colors;

        var fontSpace = (codeW - fontSize * captchaCode.Length) / (captchaCode.Length + 1);

        try
        {
            using (var bitmap = new Bitmap(codeW, codeH))
            using (var graphics = Graphics.FromImage(bitmap))
            {
                graphics.Clear(Color.White);

                var random = Unique.GetRandom();

                //画躁线
                for (int i = 0; i < settings.LineQty; i++)
                {
                    graphics.DrawLine(
                        new Pen(colors[random.Next(colors.Length)]),
                        random.Next(codeW),
                        random.Next(codeH),
                        random.Next(codeW),
                        random.Next(codeH)
                    );
                }

                //画噪点
                for (int i = 0; i < settings.PointQty; i++)
                {
                    bitmap.SetPixel(
                        random.Next(codeW),
                        random.Next(codeH),
                        colors[random.Next(colors.Length)]
                    );
                }

                //画验证码
                for (int i = 0; i < captchaCode.Length; i++)
                {
                    var font = new Font(fonts[random.Next(fonts.Length)], fontSize);
                    graphics.DrawString(
                        captchaCode[i].ToString(),
                        font,
                        new SolidBrush(colors[random.Next(colors.Length)]),
                        i * fontSize + i * fontSpace,
                        (height - font.Height) / 2f
                    );
                }

                //写入内存流
                using (var stream = new MemoryStream())
                {
                    bitmap.Save(stream, ImageFormat.Jpeg);
                    result = stream.ToArray();
                }
            }
        }
        catch
        {
            throw;
        }

        return result;
    }
}