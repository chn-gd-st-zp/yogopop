﻿global using Flurl.Http;
global using Microsoft.Extensions.DependencyInjection;
global using SimpleCaptcha.Generator;
global using System.ComponentModel;
global using System.Drawing;
global using System.Drawing.Imaging;
global using YogoPop.Core.Injection;
global using YogoPop.Core.Injection.Settings;
global using YogoPop.Core.Tool;