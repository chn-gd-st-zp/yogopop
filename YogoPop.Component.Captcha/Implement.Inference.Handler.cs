﻿namespace YogoPop.Component.Captcha;

[DIModeForService(DIModeEnum.ExclusiveByKeyed, typeof(ICaptchaHandler), CaptchaEnum.Inference)]
public class InferenceCaptcha : ICaptchaHandler
{
    private readonly InferenceCaptchaSettings _settings;

    public InferenceCaptcha(InferenceCaptchaSettings settings) { _settings = settings; }

    public async Task<object> Generate()
    {
        var result = new List<InferenceCaptchaResult>();

        try
        {
            var sourceBM = default(Bitmap);

            #region 加载图片

            var sourceImg = default(Image);

            if (_settings.SourceAddress.ToLower().Contains("http:") || _settings.SourceAddress.ToLower().Contains("https:"))
            {
                var stream = await _settings.SourceAddress
                    .WithHeaders(_settings.Headers)
                    .GetStreamAsync();
                if (stream == null)
                    return result;

                sourceImg = Bitmap.FromStream(stream);
            }
            else
            {
                if (!Directory.Exists(_settings.SourceAddress))
                    return result;

                var imgPaths = Directory.GetFiles(_settings.SourceAddress);
                if (imgPaths.Length == 0)
                    return result;

                sourceImg = Bitmap.FromFile(imgPaths[int.Parse(Unique.GetRandomCode1(0, imgPaths.Length - 1))]);
            }

            if (sourceImg == null)
                return result;

            sourceBM = new Bitmap(sourceImg.Width, sourceImg.Height, PixelFormat.Format32bppArgb);
            using (var graphics = Graphics.FromImage(sourceBM))
            {
                graphics.DrawImage(sourceImg, 0, 0);
            }

            #endregion;

            #region 裁除余量

            var startX = 0;
            var startY = 0;
            var marginW = sourceBM.Width % _settings.Columns;
            var marginH = sourceBM.Height % _settings.Rows;

            if (marginW != 0)
            {
                startX = marginW / 2;
            }

            if (marginH != 0)
            {
                startY = marginH / 2;
            }

            sourceBM = sourceBM.Clone(new RectangleF(startX, startY, sourceBM.Width, sourceBM.Height), PixelFormat.Format32bppRgb);

            #endregion

            #region 裁剪切片

            var piecesWidth = sourceBM.Width / _settings.Columns;
            var piecesHeight = sourceBM.Height / _settings.Rows;

            var index = 0;
            for (var row = 0; row < _settings.Rows; row++)
            {
                for (var col = 0; col < _settings.Columns; col++)
                {
                    var item = new InferenceCaptchaResult
                    {
                        Index = index++,
                        RowIndex = row,
                        ColIndex = col,
                    };

                    var x = col * piecesWidth;
                    var y = row * piecesHeight;

                    using (var stream = new MemoryStream())
                    {
                        var bitmap = sourceBM.Clone(new RectangleF(x, y, piecesWidth, piecesHeight), PixelFormat.Format32bppRgb);
                        bitmap.Save(stream, ImageFormat.Jpeg);
                        item.Image = stream.ToArray();
                    }

                    result.Add(item);
                }
            }

            #endregion
        }
        catch (Exception)
        {
            throw;
        }

        return result;
    }
}