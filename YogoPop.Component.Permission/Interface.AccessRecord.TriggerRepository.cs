﻿namespace YogoPop.Component.Permission;

public interface IAccessRecordTriggerRepository : IDBRepository
{
    IAccessRecordTrigger GetTriggerObj(object pk);
}