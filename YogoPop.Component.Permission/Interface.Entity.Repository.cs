﻿namespace YogoPop.Component.Permission;

public interface IPermissionRepository : IDBRepository
{
    bool Create(IEnumerable<IPermission> permissions);

    bool Delete(IEnumerable<IPermission> permissions);

    IPermission Permission(string code);

    IEnumerable<IPermission> AllPermission();
}