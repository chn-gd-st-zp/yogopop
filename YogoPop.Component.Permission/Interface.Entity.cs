﻿namespace YogoPop.Component.Permission;

public interface IPermission
{
    public bool AccessLogger { get; set; }
}