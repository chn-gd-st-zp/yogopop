﻿namespace YogoPop.Component.Permission;

public interface IAccessRecordRepository : IDBRepository
{
    bool Create(AccessRecord accessRecord);
}