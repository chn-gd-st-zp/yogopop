﻿namespace YogoPop.Component.Permission;

public class AccessRecord
{
    public RoleEnum RoleType { get; set; }

    public string AccountID { get; set; }

    public string UserName { get; set; }

    public OperationTypeEnum OperationType { get; set; }

    public string TBName { get; set; }

    public string TBValue { get; set; }

    public string PKName { get; set; }

    public string PKValue { get; set; }

    public string TriggerName { get; set; }

    public List<AccessRecordDescription> Descriptions { get; set; }

    public object ExecResult { get; set; }
}