namespace YogoPop.Component.Permission;

public interface IPermissionInitialization : ISingleton
{
    void Operation(ref IEnumerable<IPermission> permissionsFromDB, ref IEnumerable<IPermission> permissionsFromService);
}