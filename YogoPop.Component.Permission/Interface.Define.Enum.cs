﻿namespace YogoPop.Component.Permission;

public interface IPermissionEnum
{
    public Type EnumType { get; }
}