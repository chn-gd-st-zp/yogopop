namespace YogoPop.DB.Define;

public interface IDBFPrimaryKey : IDBField
{
    //
}

public interface IDBFPrimaryKey<T> : IDBFPrimaryKey
{
    public T PrimaryKey { get; set; }
}