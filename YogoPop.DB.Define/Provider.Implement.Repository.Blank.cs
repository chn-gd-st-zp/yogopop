namespace YogoPop.DB.Define;

public abstract partial class DBRepository : IDBRepository
{
    public DBRepository() { DBContext = GetDBContext(); }

    ~DBRepository() => Dispose();

    public void Dispose()
    {
        CleanUp();
        GC.SuppressFinalize(this);
    }

    protected virtual void CleanUp()
    {
        if (_disposed) return;
        _disposed = true;

        DBContext.Dispose();
    }

    private bool _disposed = false;

    public virtual IDBContext DBContext { get; protected set; }

    protected virtual IDBContext GetDBContext() { return InjectionContext.Resolve<IDBContext>(); }
}