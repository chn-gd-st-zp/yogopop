namespace YogoPop.Component.Auth;

public interface IYogoSessionContext : ITransient
{
    string ContextID { get; set; }

    string TypeOfTokenProvider { get; set; }

    OperationTypeEnum OperationType { get; set; }

    void RestoreContextID();

    void Save();
}