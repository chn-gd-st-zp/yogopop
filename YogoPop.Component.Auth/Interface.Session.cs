﻿namespace YogoPop.Component.Auth;

public interface IYogoSession : ITransient
{
    IYogoSessionContext SessionContext { get; }

    YogoSessionInfo CurrentAccount { get; }

    void Set(YogoSessionInfo info);

    YogoSessionInfo Get(string accessToken);

    void Remove(string accessToken);

    void VerifyPermission(string permissionCode);
}

public interface IYogoSession<TTokenProvider> : IYogoSession
    where TTokenProvider : ITokenProvider
{
    TTokenProvider TokenProvider { get; }
}