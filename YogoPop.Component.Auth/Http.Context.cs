﻿namespace YogoPop.Component.Auth;

public class YogoSessionContext : YogoSessionContextBase
{
    public override void RestoreContextID()
    {
        var context = InjectionContext.Resolve<IHttpContextAccessor>();
        if (context == null)
            return;

        if (context.HttpContext == null)
            return;

        ContextID = context.HttpContext.TraceIdentifier;
    }
}