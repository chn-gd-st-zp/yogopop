namespace YogoPop.Component.Auth;

public class YogoSessionContextFactory
{
    public static IYogoSessionContext RestoreSessionContext()
    {
        var context = default(IYogoSessionContext);

        foreach (var kv in typeof(ProtocolEnum).ToDictionary())
        {
            context = InjectionContext.ResolveByKeyed<IYogoSessionContext>(kv.Key.ToEnum<ProtocolEnum>());
            if (context != null)
                break;
        }

        if (context != null)
        {
            context.RestoreContextID();

            var authSettings = InjectionContext.Resolve<AuthSettings>();
            using (var cache = authSettings.ResolveCache<ICache4Redis>())
            {
                var contextInCache = cache.Get<YogoSessionContextBase>(authSettings.SessionPrefix + context.ContextID);
                if (contextInCache != null)
                    context = contextInCache;
            }
        }

        return context;
    }
}

public class YogoSessionContextBase : IYogoSessionContext
{
    public virtual string ContextID { get; set; }

    public virtual string TypeOfTokenProvider { get; set; }

    public virtual OperationTypeEnum OperationType { get; set; }

    public virtual void RestoreContextID() { }

    public void Save()
    {
        var authSettings = InjectionContext.Resolve<AuthSettings>();
        using (var cache = authSettings.ResolveCache<ICache4Redis>())
        {
            cache.Set(authSettings.SessionPrefix + ContextID, this.ToJson(), TimeSpan.FromMinutes(5));
        }
    }
}