﻿namespace YogoPop.Component.Auth;

public interface ITokenProvider : ITransient
{
    ProtocolEnum Protocol { get; }

    string CurrentToken { get; }
}