global using Autofac;
global using AutoMapper.Internal;
global using Mapster;
global using Microsoft.Data.SqlClient;
global using Microsoft.EntityFrameworkCore;
global using Microsoft.EntityFrameworkCore.ChangeTracking;
global using Microsoft.EntityFrameworkCore.Metadata.Builders;
global using System.ComponentModel.DataAnnotations.Schema;
global using System.Data;
global using System.Data.Common;
global using System.Linq.Expressions;
global using System.Reflection;
global using YogoPop.Core.CusEnum;
global using YogoPop.Core.DTO;
global using YogoPop.Core.Extension;
global using YogoPop.Core.Injection;
global using YogoPop.Core.Tool;
global using YogoPop.DB.Define;