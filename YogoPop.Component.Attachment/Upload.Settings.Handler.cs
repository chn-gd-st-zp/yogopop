namespace YogoPop.Component.Attachment;

public class AttachmentHandlerSetting
{
    public AttachmentHandlerEnum Handler { get; set; }

    public string[] Exts { get; set; }
}