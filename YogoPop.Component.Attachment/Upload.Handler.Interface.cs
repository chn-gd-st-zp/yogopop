namespace YogoPop.Component.Attachment;

public interface IHandler : ITransient
{
    AttachmentResultEnum Do(AttachmentOperationItemSetting operationItemSetting, string path, string fileName, string fileExt);
}