namespace YogoPop.Component.Attachment;

public static class ExportExcelExtension
{
    public static DataTable ToDataTable<T>(this List<T> objList) where T : new()
    {
        objList = objList == null ? new List<T>() : objList;

        DataTable result = null;
        T data_aggregate = default;

        var sheetAttr = default(ExportSheetAttribute);
        var cols = new List<ExportColAttribute>();
        var pi_content = new List<PropertyInfo>();
        var pi_aggregate = new List<PropertyInfo>();

        ExportColAttribute col = null;
        DataRow row_content = null;
        DataRow row_aggregate = null;

        #region 初始化

        Type type = typeof(T);

        sheetAttr = type.GetCustomAttribute<ExportSheetAttribute>();
        if (sheetAttr == null)
            sheetAttr = new ExportSheetAttribute(string.Empty);

        PropertyInfo[] piArray = type.GetProperties();
        if (piArray == null)
            return result;

        result = new DataTable(sheetAttr.SheetName);
        data_aggregate = new T();

        foreach (var pi1 in piArray)
        {
            col = pi1.GetCustomAttribute<ExportContentAttribute>(false);
            if (col != null)
            {
                cols.Add(col);
                pi_content.Add(pi1);
            }

            col = pi1.GetCustomAttribute<ExportAggregateAttribute>(false);
            if (col != null)
            {
                cols.Add(col);
                pi_aggregate.Add(pi1);

                foreach (var obj in objList)
                {
                    Type type2 = obj.GetType();

                    foreach (var pi2 in piArray)
                    {
                        if (pi1.Name != pi2.Name)
                            continue;

                        var obj1 = pi1.GetValue(data_aggregate, null);
                        var obj2 = pi2.GetValue(obj, null);
                        var dataValue = col.Calculate(obj1, obj2);
                        pi1.SetValue(data_aggregate, dataValue);
                    }
                }
            }
        }

        cols = cols.GroupBy(o => o.Index).Select(o => o.First()).ToList();

        if (cols.IsEmpty())
            return result;

        #endregion

        #region 数据填充

        foreach (var titleCol in cols)
            result.Columns.Add(titleCol.Title, typeof(object));

        foreach (var obj in objList)
        {
            row_content = result.NewRow();

            foreach (var pi in pi_content)
            {
                col = pi.GetCustomAttribute<ExportContentAttribute>(false);
                if (col != null)
                {
                    var colData = pi.GetValue(obj, null);
                    row_content[col.Title] = col.GetDataValue(colData);
                }
            }

            result.Rows.Add(row_content);
        }

        row_aggregate = result.NewRow();

        foreach (var pi in pi_aggregate)
        {
            col = pi.GetCustomAttribute<ExportAggregateAttribute>(false);
            if (col != null)
            {
                var colData = pi.GetValue(data_aggregate, null);
                row_aggregate[col.Title] = col.GetDataValue(colData);
            }
        }

        result.Rows.Add(row_aggregate);

        #endregion

        return result;
    }

    public static List<T> ToObject<T>(this Stream stream) where T : class, new()
    {
        var result = new List<T>();
        ExcelPackage.LicenseContext = OfficeOpenXml.LicenseContext.NonCommercial;
        var sheetAttr = default(ImportSheetAttribute);
        var cols = new Dictionary<ImportColAttribute, PropertyInfo>();
        var datas = new List<Dictionary<ImportColAttribute, object>>();

        #region 初始化 列标识 从泛型对象载入

        Type type = typeof(T);

        sheetAttr = type.GetCustomAttribute<ImportSheetAttribute>();
        if (sheetAttr == null)
            return result;

        PropertyInfo[] piArray = type.GetProperties();
        if (piArray == null)
            return result;

        foreach (var pi in piArray)
        {
            var attr = pi.GetCustomAttribute<ImportColAttribute>(false);
            if (attr == null)
                continue;

            cols.Add(attr, pi);
        }

        #endregion

        #region 初始化 数据内容 从文件载入

        using (var package = new ExcelPackage(stream))
        {
            if (package == null) return result;
            if (package.Workbook == null) return result;
            if (package.Workbook.Worksheets == null) return result;

            var sheet = package.Workbook.Worksheets[sheetAttr.SheetName];
            if (sheet == null) return result;

            var dimension = sheet.Dimension;
            if (dimension == null) return result;

            int rowQty = dimension.Rows;
            int colQty = dimension.Columns;

            if (colQty != cols.Count()) return result;

            try
            {
                //sheet.Cells的 [行] 和 [列] 的索引都是从1开始的

                //跳过第一行的列名
                for (int curRowIndex = 2; curRowIndex <= rowQty; curRowIndex++)
                {
                    var data = new Dictionary<ImportColAttribute, object>();

                    for (int curColIndex = 1; curColIndex <= colQty; curColIndex++)
                    {
                        var colIndex = 0;
                        var colAttr = cols.Keys.ToList()[curColIndex - 1];
                        var colTitle = !colAttr.ColAlias.IsEmptyString() ? colAttr.ColAlias : cols[colAttr].Name;

                        #region 每一列的去查，获取当前列所对应的属性的确切下标

                        for (int index = 1; index <= colQty; index++)
                        {
                            var title = sheet.Cells[1, index].Value;
                            if (colTitle.IsEquals(title == null ? string.Empty : title.ToString()))
                            {
                                colIndex = index;
                                break;
                            }
                        }

                        #endregion

                        var colValue = sheet.Cells[curRowIndex, colIndex].Value;

                        data.Add(colAttr, colValue);
                    }

                    datas.Add(data);
                }
            }
            catch (Exception ex)
            {
                throw new Exception("初始化数据内容发生错误", ex);
            }
        }

        #endregion

        foreach (var data in datas)
        {
            var resultItem = new T();

            foreach (var col in cols)
            {
                object value = Convert.ChangeType(data[col.Key], col.Key.ColType);
                col.Value.SetValue(resultItem, value);
            }

            result.Add(resultItem);
        }

        return result;
    }
}