﻿namespace YogoPop.Component.VerificationCode;

public interface IVCSettings
{
    public int DurationSecond { get; set; }
}