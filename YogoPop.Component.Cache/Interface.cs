namespace YogoPop.Component.Cache;

public interface ICacheSettings : ISettings
{
    public int DBIndex { get; set; }

    public string Prefix { get; set; }
}

public interface ICache : IDisposable
{
    public List<string> Keys(string pattern = "*");

    public List<T> List<T>(string pattern = "*");

    public bool Expire(string key);

    public bool Expire(string key, TimeSpan ts);

    public bool Expire(string key, DateTime dt);

    public bool Expire(string key, int second);

    public bool Set<T>(string key, T value);

    public bool Set<T>(string key, T value, TimeSpan ts);

    public bool Set<T>(string key, T value, DateTime dt);

    public bool Set<T>(string key, T value, int second);

    public T Get<T>(string key);

    public bool Exists(string key);

    public bool Del(string key);
}

public interface ICache4Redis : ICache
{
    public T GetClient<T>(int? dbIndex = null);

    public List<string> Keys(string pattern = "*", int? dbIndex = null);

    public List<T> List<T>(string pattern = "*", int? dbIndex = null);

    public bool Expire(string key, int? dbIndex = null);

    public bool Expire(string key, TimeSpan ts, int? dbIndex = null);

    public bool Expire(string key, DateTime dt, int? dbIndex = null);

    public bool Expire(string key, int second, int? dbIndex = null);

    public bool ListRightPush(string key, string obj, DateTime? expiredTime = null, int? dbIndex = null);

    public bool ListRemove(string key, string obj, long count = 0, int? dbIndex = null);

    public List<T> ListRange<T>(string key, int? dbIndex = null);

    public bool Set<T>(string key, T value, int? dbIndex = null);

    public bool Set<T>(string key, T value, TimeSpan ts, int? dbIndex = null);

    public bool Set<T>(string key, T value, DateTime dt, int? dbIndex = null);

    public bool Set<T>(string key, T value, int second, int? dbIndex = null);

    public bool Del(string key, int? dbIndex = null);

    public T Get<T>(string key, int? dbIndex = null);

    public bool Exists(string key, int? dbIndex = null);

    public bool HSet<T>(string key, string field, T value, int? dbIndex = null);

    public bool HSet<T>(string key, Dictionary<string, T> fields, int? dbIndex = null);

    public long HDel(string key, int? dbIndex = null, params string[] fields);

    public T HGet<T>(string key, string field, int? dbIndex = null);

    public Dictionary<string, T> HGet<T>(string key, int? dbIndex = null);

    public Dictionary<string, T> HGet<T>(string key, int? dbIndex = null, params string[] fields);

    public bool HExists(string key, string field, int? dbIndex = null);

    public long HIncrease(string key, string field, long increment = 1, int? dbIndex = null);

    public long HDecrease(string key, string field, long decrement = 1, int? dbIndex = null);
}