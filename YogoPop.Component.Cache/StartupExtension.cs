namespace YogoPop.Component.Cache;

public static partial class StartupExtension
{
    public static ContainerBuilder RegisRedis<TRedis>(this ContainerBuilder containerBuilder) where TRedis : ICache4Redis
    {
        containerBuilder.RegisCacheProvider<TRedis>();
        containerBuilder.RegisterType<TRedis>().As<TRedis>().InstancePerDependency();
        containerBuilder.RegisterType<TRedis>().As<ICache>().InstancePerDependency();
        containerBuilder.RegisterType<TRedis>().As<ICache4Redis>().InstancePerDependency();

        return containerBuilder;
    }

    public static ContainerBuilder RegisRedis<TRedis, TKeyed>(this ContainerBuilder containerBuilder) where TRedis : ICache4Redis
    {
        containerBuilder.RegisCacheProvider<TRedis>();
        containerBuilder.RegisterType<TRedis>().Keyed<TRedis>(typeof(TKeyed)).InstancePerDependency();
        containerBuilder.RegisterType<TRedis>().Keyed<ICache>(typeof(TKeyed)).InstancePerDependency();
        containerBuilder.RegisterType<TRedis>().Keyed<ICache4Redis>(typeof(TKeyed)).InstancePerDependency();

        return containerBuilder;
    }

    public static TCache ResolveCache<TCache>(this ICacheSettings settings, bool fullyCover = false) where TCache : ICache
    {
        if (settings == null) return default;

        var redisSettings_source = settings.ToJson().ToObject<RedisSettings>();
        var redisSettings_using = InjectionContext.Resolve<RedisSettings>().DeepCopy();

        redisSettings_using.DBIndex = redisSettings_source.DBIndex;
        redisSettings_using.Prefix = fullyCover ? redisSettings_source.Prefix : redisSettings_using.Prefix + redisSettings_source.Prefix;

        //return InjectionContext.ResolveByKeyed<TCache>(settings.GetType(), new NamedPropertyParameter("redisSettings", redisSettings), new NamedPropertyParameter("defaultDatabase", redisSettings.DBIndex));
        //return InjectionContext.ResolveByKeyed<TCache>(settings.GetType(), redisSettings.ToNamedPropertyParameter("redisSettings"), redisSettings.DBIndex.ToNamedPropertyParameter("defaultDatabase"));

        //return InjectionContext.ResolveByKeyed<TCache>(settings.GetType(),  new TypedParameter(typeof(RedisSettings), redisSettings), new TypedParameter(typeof(int), redisSettings.DBIndex));
        return InjectionContext.ResolveByKeyed<TCache>(settings.GetType(), redisSettings_using.ToTypedParameter<RedisSettings>(), redisSettings_using.DBIndex.ToTypedParameter<int>());
    }

    private static ContainerBuilder RegisCacheProvider<TCache>(this ContainerBuilder containerBuilder) where TCache : ICache
    {
        var type = typeof(TCache);
        var attr = type.GetCustomAttribute<CacheProviderAttribute>();
        if (attr == null) return containerBuilder;

        containerBuilder.RegisterType(attr.Type).As(attr.Type).SingleInstance();

        return containerBuilder;
    }
}