namespace YogoPop.Component.Cache;

public class CacheProviderAttribute : Attribute
{
    public Type Type { get; set; }

    public CacheProviderAttribute(Type type) { Type = type; }
}