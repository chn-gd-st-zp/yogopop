﻿namespace YogoPop.Component.Logger;

public interface INLogger : IYogoLogger { }

public interface INLogger<TTrigger> : INLogger, IYogoLogger<TTrigger> where TTrigger : class { }