namespace YogoPop.Component.Logger;

//public interface ISeriLogger : IYogoLogger, Seri.ILogger { }

public interface ISeriLogger : IYogoLogger { }

public interface ISeriLogger<TTrigger> : IYogoLogger<TTrigger>, ISeriLogger where TTrigger : class { }