public delegate RedisClient WSRedisClient();

public static class WSExtension
{
    static bool isUseWebSockets = false;

    /// <summary>
    /// 启用 ImServer 服务端
    /// </summary>
    /// <param name="app"></param>
    /// <param name="options"></param>
    /// <returns></returns>
    public static IApplicationBuilder UseFreeImServer(this IApplicationBuilder app, WSServerOptions options)
    {
        app.Map($"/{options.PathMatch}", appcur =>
        {
            var imserv = new WSServer(options);
            if (isUseWebSockets == false)
            {
                isUseWebSockets = true;
                appcur.UseWebSockets();
            }
            appcur.Use((ctx, next) =>
                imserv.Acceptor(ctx, next));
        });
        return app;
    }
}

public class WSSendEventArgs : EventArgs
{
    /// <summary>
    /// 发送者的客户端id
    /// </summary>
    public Guid SenderClientId { get; }

    /// <summary>
    /// 接收者的客户端id
    /// </summary>
    public List<Guid> ReceiveClientId { get; } = new List<Guid>();

    /// <summary>
    /// imServer 服务器节点
    /// </summary>
    public string Server { get; }

    /// <summary>
    /// 消息
    /// </summary>
    public object Message { get; }

    /// <summary>
    /// 是否回执
    /// </summary>
    public bool Receipt { get; }

    internal WSSendEventArgs(string server, Guid senderClientId, object message, bool receipt = false)
    {
        this.Server = server;
        this.SenderClientId = senderClientId;
        this.Message = message;
        this.Receipt = receipt;
    }
}