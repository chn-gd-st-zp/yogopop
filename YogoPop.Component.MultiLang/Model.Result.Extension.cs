namespace YogoPop.Component.MultiLang;

public static class ServiceResultExtension
{
    public static IServiceResult<TResult> Success<TResult, TMultiLanguageObject>(this TResult data) where TMultiLanguageObject : IMultiLanguageObject
    {
        var mlo = InstanceCreator.Create<TMultiLanguageObject>();

        var result = new MultilangServiceResult<TResult>()
        {
            IsSuccess = true,
            ExInfo = null,
            Code = IVEnum.Restore<IStateCode>().Success,
            Data = data,

            GroupKey = mlo != null ? mlo.GroupKey : string.Empty,
            ItemKey = mlo != null ? mlo.ItemKey : string.Empty,
        };

        return result;
    }

    public static IServiceResult<TResult> Fail<TResult, TMultiLanguageObject>(this TResult data) where TMultiLanguageObject : IMultiLanguageObject
    {
        var mlo = InstanceCreator.Create<TMultiLanguageObject>();

        var result = new MultilangServiceResult<TResult>()
        {
            IsSuccess = false,
            ExInfo = null,
            Code = IVEnum.Restore<IStateCode>().Fail,
            Data = data,

            GroupKey = mlo != null ? mlo.GroupKey : string.Empty,
            ItemKey = mlo != null ? mlo.ItemKey : string.Empty,
        };

        return result;
    }

    public static IServiceResult<TResult2> MLTransfer<TResult1, TResult2>(this IServiceResult<TResult1> serviceResult, TResult2 newData = default)
    {
        var mlo = serviceResult as IMultiLanguageObject;

        var result = new MultilangServiceResult<TResult2>
        {
            IsSuccess = serviceResult.IsSuccess,
            Code = serviceResult.Code,
            Msg = serviceResult.Msg,
            Data = newData,

            GroupKey = mlo != null ? mlo.GroupKey : string.Empty,
            ItemKey = mlo != null ? mlo.ItemKey : string.Empty,
        };

        return result;
    }

    public static IApiResult<TResult> ToMLApiResult<TResult>(this IServiceResult<TResult> serviceResult)
    {
        var mlo = serviceResult as IMultiLanguageObject;

        var result = new MultilangApiResult<TResult>
        {
            Code = serviceResult.Code.Value,
            Msg = serviceResult.Msg,
            Data = serviceResult.Data,

            GroupKey = mlo != null ? mlo.GroupKey : string.Empty,
            ItemKey = mlo != null ? mlo.ItemKey : string.Empty,
        };

        return result;
    }
}