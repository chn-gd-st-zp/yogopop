namespace YogoPop.Component.MultiLang;

public class MLActionResultExecutor : IActionResultExecutor
{
    public object Execute(object data)
    {
        if (data == null) return data;

        var defaultSettings = InjectionContext.Resolve<MultilangDefaultSettings>();
        if (defaultSettings == null) return data;

        var httpContext = InjectionContext.Resolve<IHttpContextAccessor>();
        if (httpContext == null) return data;

        var headers = httpContext.HttpContext.Request.Headers;
        var language = headers.ContainsKey(AppInitHelper.LanguageKeyInHeader, true) ? headers[AppInitHelper.LanguageKeyInHeader].ToString() : defaultSettings.Language;

        var type = data.GetType();
        if (!type.IsImplementedOf<IMultiLanguageObject>()) return data;
        if (!type.IsImplementedOf<IApiResult>()) return data;

        var data_mlo = data as IMultiLanguageObject;
        if (data_mlo == null) return data;

        var data_api = data as IApiResult;
        if (data_api == null) return data;

        var groupKey = data_mlo.GroupKey.IsNotEmptyString() ? data_mlo.GroupKey : string.Empty;
        var itemKey = data_mlo.ItemKey.IsNotEmptyString() ? data_mlo.ItemKey : string.Empty;
        var contentData = InjectionContext.Resolve<IMultilangExchanger>().GetAsync(language, groupKey, itemKey).Result;

        data_api.Msg = contentData != null ? contentData.DestContent : data_api.Msg;
        data_api.Msg = data_api.Msg.IsNotEmptyString() ? data_api.Msg : $"{groupKey}-{itemKey}-MLNoMapping";

        return data_api;
    }
}