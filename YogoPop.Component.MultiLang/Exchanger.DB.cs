namespace YogoPop.Component.MultiLang;

public interface IMultilangDBExchanger : IMultilangExchanger, ITransient
{
    //
}

public interface IMultilangDBContext : IDBContext { }