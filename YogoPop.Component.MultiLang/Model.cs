namespace YogoPop.Component.MultiLang;

public interface IMultiLanguageObject
{
    public string GroupKey { get; }

    public string ItemKey { get; }
}